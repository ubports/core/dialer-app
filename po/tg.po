# Tajik translation for dialer-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the dialer-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: dialer-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-02 13:14+0000\n"
"PO-Revision-Date: 2015-03-10 04:50+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Tajik <tg@li.org>\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2017-02-17 05:52+0000\n"
"X-Generator: Launchpad (build 18326)\n"

#: src/lomiri-dialer-app.desktop.in:4 src/qml/DialerPage/DialerPage.qml:165
msgid "Phone"
msgstr ""

#: src/lomiri-dialer-app.desktop.in:5
msgid "Phone App"
msgstr ""

#: src/lomiri-dialer-app.desktop.in:6
msgid "Phone application"
msgstr ""

#: src/lomiri-dialer-app.desktop.in:7
msgid "Phone;Dialer;Dial;Call;Keypad"
msgstr ""

#: src/qml/SettingsPage/dateUtils.js:41 src/qml/HistoryPage/dateUtils.js:45
msgid "Today"
msgstr ""

#: src/qml/SettingsPage/dateUtils.js:43 src/qml/HistoryPage/dateUtils.js:47
msgid "Yesterday"
msgstr ""

#: src/qml/SettingsPage/dateUtils.js:61 src/qml/HistoryPage/dateUtils.js:92
#, qt-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] ""
msgstr[1] ""

#: src/qml/SettingsPage/dateUtils.js:63 src/qml/HistoryPage/dateUtils.js:94
#, qt-format
msgid "%1 min"
msgid_plural "%1 mins"
msgstr[0] ""
msgstr[1] ""

#: src/qml/SettingsPage/dateUtils.js:65 src/qml/HistoryPage/dateUtils.js:96
#, qt-format
msgid "%1 sec"
msgid_plural "%1 secs"
msgstr[0] ""
msgstr[1] ""

#. TRANSLATORS: this is the duration time format when the call lasted more than an hour
#: src/qml/HistoryPage/dateUtils.js:112
msgid "hh:mm:ss"
msgstr ""

#. TRANSLATORS: this is the duration time format when the call lasted less than an hour
#: src/qml/HistoryPage/dateUtils.js:115
msgid "mm:ss"
msgstr ""

#: src/qml/dialer-app.qml:387
msgid "No SIM card selected"
msgstr ""

#: src/qml/dialer-app.qml:387
msgid "You need to select a SIM card"
msgstr ""

#: src/qml/dialer-app.qml:406 src/qml/dialer-app.qml:411
#: src/qml/DialerPage/DialerPage.qml:167
msgid "No network"
msgstr ""

#: src/qml/dialer-app.qml:406 src/qml/dialer-app.qml:413
msgid "There is currently no network."
msgstr ""

#: src/qml/dialer-app.qml:412
#, qt-format
msgid "There is currently no network on %1"
msgstr ""

#: src/qml/dialer-app.qml:619
msgid "Call Barring"
msgstr ""

#: src/qml/dialer-app.qml:630
msgid "Call Forwarding"
msgstr ""

#: src/qml/dialer-app.qml:641
msgid "Call Waiting"
msgstr ""

#: src/qml/dialer-app.qml:652
msgid "Calling Line Presentation"
msgstr ""

#: src/qml/dialer-app.qml:658
msgid "Connected Line Presentation"
msgstr ""

#: src/qml/dialer-app.qml:664
msgid "Calling Line Restriction"
msgstr ""

#: src/qml/dialer-app.qml:670
msgid "Connected Line Restriction"
msgstr ""

#: src/qml/LiveCallPage/ConferenceCallDisplay.qml:73
#: src/qml/LiveCallPage/MultiCallDisplay.qml:91
msgid "Voicemail"
msgstr ""

#: src/qml/LiveCallPage/ConferenceCallDisplay.qml:113
#: src/qml/LiveCallPage/LiveCall.qml:490
#: src/qml/LiveCallPage/MultiCallDisplay.qml:108
#: src/qml/DialerPage/DialerPage.qml:606
msgid "Calling"
msgstr ""

#: src/qml/LiveCallPage/ConferenceCallDisplay.qml:141
msgid "Private"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:51
#: src/qml/LiveCallPage/MultiCallDisplay.qml:89
msgid "Conference"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:129
msgid "No calls"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:144
msgid "Conference call failure"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:145
msgid "Failed to create a conference call."
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:168
msgid "Call ended"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:177
msgid "Call holding failure"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:178
msgid "Failed to activate the call."
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:179
msgid "Failed to place the active call on hold."
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:194
msgid "Switch audio source:"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:315
msgid "Call failed"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:389
msgid "Bluetooth device"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:391
msgid "Ubuntu Touch Phone"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:393
msgid "Phone Speaker"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:395
msgid "Unknown device"
msgstr ""

#. TRANSLATORS: %1 is the call duration here.
#: src/qml/LiveCallPage/LiveCall.qml:488
#, qt-format
msgid "%1 - on hold"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:592
msgid "Switch calls"
msgstr ""

#: src/qml/LiveCallPage/LiveCall.qml:607
msgid "Merge calls"
msgstr ""

#: src/qml/LiveCallPage/MultiCallDisplay.qml:129
msgid "On hold"
msgstr ""

#: src/qml/LiveCallPage/MultiCallDisplay.qml:131
msgid "Active"
msgstr ""

#: src/qml/ContactEditorPage/DialerContactEditorPage.qml:35
#: src/qml/SettingsPage/CallForwarding.qml:236
#: src/qml/SettingsPage/OnlineAccountsHelper.qml:78
#: src/qml/ContactsPage/ContactsPage.qml:170
#: src/qml/HistoryPage/HistoryCleaner.qml:138
#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:46
#: src/qml/Dialogs/DisableFlightModeDialog.qml:45
msgid "Cancel"
msgstr ""

#: src/qml/ContactEditorPage/DialerContactEditorPage.qml:47
msgid "Save"
msgstr ""

#: src/qml/MMI/IMEIDialog.qml:27
msgid "IMEI"
msgstr ""

#: src/qml/MMI/IMEIDialog.qml:40 src/qml/Dialogs/UssdResponseDialog.qml:30
#: src/qml/Dialogs/UssdErrorDialog.qml:31
msgid "Dismiss"
msgstr ""

#: src/qml/SettingsPage/SettingsPage.qml:29
#: src/qml/DialerPage/DialerPage.qml:71
msgid "Settings"
msgstr ""

#: src/qml/SettingsPage/SettingsPage.qml:113
msgid "Dialpad tones"
msgstr ""

#: src/qml/SettingsPage/SettingsPage.qml:123
msgid "Add an online account"
msgstr ""

#: src/qml/SettingsPage/SettingsPage.qml:136
msgid "Contact search with dial pad (Experimental)"
msgstr ""

#: src/qml/SettingsPage/CallWaiting.qml:31
#: src/qml/SettingsPage/CallWaiting.qml:86
#: src/qml/SettingsPage/SingleSim.qml:32 src/qml/SettingsPage/NoSims.qml:34
#: src/qml/SettingsPage/MultiSim.qml:41
msgid "Call waiting"
msgstr ""

#: src/qml/SettingsPage/CallWaiting.qml:101
msgid ""
"Lets you answer or start a new call while on another call, and switch "
"between them"
msgstr ""

#. TRANSLATORS: %1 is the displayname of the account
#: src/qml/SettingsPage/AccountSettings/SipNumberRewrite.qml:30
#: src/qml/SettingsPage/AccountSettings/sip.qml:26
#, qt-format
msgid "%1 Number Rewrite"
msgstr ""

#: src/qml/SettingsPage/AccountSettings/SipNumberRewrite.qml:88
msgid "Number rewrite"
msgstr ""

#: src/qml/SettingsPage/AccountSettings/SipNumberRewrite.qml:96
msgid "Default country code"
msgstr ""

#: src/qml/SettingsPage/AccountSettings/SipNumberRewrite.qml:100
msgid "Enter a country code"
msgstr ""

#: src/qml/SettingsPage/AccountSettings/SipNumberRewrite.qml:118
msgid "Default area code"
msgstr ""

#: src/qml/SettingsPage/AccountSettings/SipNumberRewrite.qml:122
msgid "Enter an area code"
msgstr ""

#: src/qml/SettingsPage/AccountSettings/SipNumberRewrite.qml:138
msgid "Characters to remove"
msgstr ""

#: src/qml/SettingsPage/AccountSettings/SipNumberRewrite.qml:142
msgid "Enter the characters to remove"
msgstr ""

#: src/qml/SettingsPage/AccountSettings/SipNumberRewrite.qml:159
msgid "Prefix"
msgstr ""

#: src/qml/SettingsPage/AccountSettings/SipNumberRewrite.qml:163
msgid "Enter a prefix"
msgstr ""

#: src/qml/SettingsPage/AccountSettings/sip.qml:28
msgid "On"
msgstr ""

#: src/qml/SettingsPage/AccountSettings/sip.qml:28
#: src/qml/SettingsPage/CallForwarding.qml:382
msgid "Off"
msgstr ""

#: src/qml/SettingsPage/SingleSim.qml:28
msgid "SIM"
msgstr ""

#: src/qml/SettingsPage/SingleSim.qml:39 src/qml/SettingsPage/NoSims.qml:28
#: src/qml/SettingsPage/CallForwarding.qml:44
#: src/qml/SettingsPage/MultiSim.qml:51
msgid "Call forwarding"
msgstr ""

#. TRANSLATORS: %1 is the name of the (network) carrier
#: src/qml/SettingsPage/SingleSim.qml:51 src/qml/SettingsPage/Services.qml:34
#, qt-format
msgid "%1 Services"
msgstr ""

#: src/qml/SettingsPage/NoSims.qml:42 src/qml/SettingsPage/MultiSim.qml:62
msgid "Services"
msgstr ""

#. TRANSLATORS: This string will be truncated on smaller displays.
#: src/qml/SettingsPage/CallForwardItem.qml:157
#: src/qml/SettingsPage/CallForwardItem.qml:202
msgid "Forward to"
msgstr ""

#: src/qml/SettingsPage/CallForwardItem.qml:171
#: src/qml/DialerPage/DialerPage.qml:332
msgid "Enter a number"
msgstr ""

#: src/qml/SettingsPage/CallForwardItem.qml:218
msgid "Call forwarding can't be changed right now."
msgstr ""

#: src/qml/SettingsPage/ServiceInfo.qml:75
#, qt-format
msgid "Last called %1"
msgstr ""

#: src/qml/SettingsPage/ServiceInfo.qml:85
msgid "Call"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:127
msgid "Forward every incoming call"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:142
msgid "Redirects all phone calls to another number."
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:154
msgid "Call forwarding status can't be checked "
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:162
msgid "Forward incoming calls when:"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:171
msgid "I'm on another call"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:182
msgid "I don't answer"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:193
msgid "My phone is unreachable"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:223
msgid "Contacts..."
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:249
msgid "Set"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:270
msgid "Please select a phone number"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:279
msgid "Numbers"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:298
msgid "Could not forward to this contact"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:299
msgid "Contact not associated with any phone number."
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:301
#: src/qml/HistoryPage/HistoryCleaner.qml:156
msgid "OK"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:378
msgid "All calls"
msgstr ""

#: src/qml/SettingsPage/CallForwarding.qml:380
msgid "Some calls"
msgstr ""

#: src/qml/SettingsPage/OnlineAccountsHelper.qml:46
msgid "Pick an account to create."
msgstr ""

#: src/qml/DialerPage/Keypad.qml:58
msgid "1"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:74
msgid "2"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:75
msgid "ABC"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:90
msgid "3"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:91
msgid "DEF"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:106
msgid "4"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:107
msgid "GHI"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:122
msgid "5"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:123
msgid "JKL"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:138
msgid "6"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:139
msgid "MNO"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:154
msgid "7"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:155
msgid "PQRS"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:170
msgid "8"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:171
msgid "TUV"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:186
msgid "9"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:187
msgid "WXYZ"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:204
msgid "*"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:219
msgid "0"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:220 src/qml/DialerPage/DialerBottomEdge.qml:26
msgid "+"
msgstr ""

#: src/qml/DialerPage/Keypad.qml:238
msgid "#"
msgstr ""

#: src/qml/DialerPage/DialerPage.qml:60
msgid "Favorite Contacts"
msgstr ""

#: src/qml/DialerPage/DialerPage.qml:66
#: src/qml/ContactsPage/ContactsPage.qml:102
msgid "Contacts"
msgstr ""

#: src/qml/DialerPage/DialerPage.qml:93
#: src/qml/Dialogs/NotificationDialog.qml:29
msgid "Close"
msgstr ""

#: src/qml/DialerPage/DialerPage.qml:145
msgid "Initializing..."
msgstr ""

#: src/qml/DialerPage/DialerPage.qml:148 src/qml/DialerPage/DialerPage.qml:158
msgid "Emergency Calls"
msgstr ""

#: src/qml/DialerPage/DialerPage.qml:153
#: src/qml/Dialogs/DisableFlightModeDialog.qml:27
msgid "Flight Mode"
msgstr ""

#: src/qml/DialerPage/DialerPage.qml:156
msgid "SIM Locked"
msgstr ""

#: src/qml/DialerPage/DialerPage.qml:564
msgid "Emergency call"
msgstr ""

#: src/qml/DialerPage/DialerPage.qml:564
msgid "This is not an emergency number."
msgstr ""

#: src/qml/DialerPage/DialerPage.qml:619 src/qml/HistoryPage/HistoryPage.qml:47
msgid "Recent"
msgstr ""

#: src/qml/ContactsPage/ContactsPage.qml:92
msgid "Search..."
msgstr ""

#: src/qml/ContactsPage/ContactsPage.qml:120
msgctxt "All Contacts"
msgid "All"
msgstr ""

#: src/qml/ContactsPage/ContactsPage.qml:120
msgid "Favorites"
msgstr ""

#: src/qml/ContactsPage/ContactsPage.qml:144
msgid "Search"
msgstr ""

#: src/qml/ContactsPage/ContactsPage.qml:250
msgid "view contact"
msgstr ""

#: src/qml/ContactViewPage/DialerContactViewPage.qml:61
#: src/qml/HistoryPage/HistoryDetailsPage.qml:83
msgid "Share"
msgstr ""

#: src/qml/ContactViewPage/DialerContactViewPage.qml:70
msgid "Edit"
msgstr ""

#: src/qml/HistoryPage/HistoryPage.qml:47
msgid "Select"
msgstr ""

#: src/qml/HistoryPage/HistoryPage.qml:68
msgctxt "All Calls"
msgid "All"
msgstr ""

#: src/qml/HistoryPage/HistoryPage.qml:68
#: src/qml/HistoryPage/HistoryDelegate.qml:63
#: src/qml/HistoryPage/HistoryDetailsPage.qml:338
msgid "Missed"
msgstr ""

#: src/qml/HistoryPage/HistoryPage.qml:86
msgid "Clear history"
msgstr ""

#: src/qml/HistoryPage/HistoryPage.qml:276
msgid "No recent calls"
msgstr ""

#: src/qml/HistoryPage/HistoryPage.qml:372
#: src/qml/HistoryPage/HistoryCleaner.qml:146
#: src/qml/HistoryPage/HistoryDetailsPage.qml:91
#: src/qml/HistoryPage/HistoryDetailsPage.qml:263
msgid "Delete"
msgstr ""

#: src/qml/HistoryPage/HistoryPage.qml:381
msgid "Details"
msgstr ""

#: src/qml/HistoryPage/HistoryPage.qml:391
msgid "Send message"
msgstr ""

#: src/qml/HistoryPage/HistoryPage.qml:400
#: src/qml/HistoryPage/HistoryDetailsPage.qml:71
msgid "Contact Details"
msgstr ""

#: src/qml/HistoryPage/HistoryCleaner.qml:29
msgid "Call history"
msgstr ""

#: src/qml/HistoryPage/HistoryCleaner.qml:60
msgid "Remove all history older than:"
msgstr ""

#: src/qml/HistoryPage/HistoryCleaner.qml:65
#, qt-format
msgid "%1 records to clean"
msgstr ""

#: src/qml/HistoryPage/HistoryCleaner.qml:70
msgid "No records to clean"
msgstr ""

#: src/qml/HistoryPage/HistoryCleaner.qml:75
#, qt-format
msgid "Deleting %1 records..."
msgstr ""

#: src/qml/HistoryPage/HistoryCleaner.qml:81
#, qt-format
msgid "Removed %1 records"
msgstr ""

#: src/qml/HistoryPage/HistoryCleaner.qml:87
#, qt-format
msgid "Removed %1 records, sorry, something went wrong."
msgstr ""

#: src/qml/HistoryPage/HistoryCleaner.qml:111
#: src/qml/HistoryPage/HistoryCleaner.qml:112
#: src/qml/HistoryPage/HistoryCleaner.qml:113
#, qt-format
msgid "%1 month"
msgid_plural "%1 months"
msgstr[0] ""
msgstr[1] ""

#: src/qml/HistoryPage/HistoryCleaner.qml:114
msgid "1 year"
msgstr ""

#: src/qml/HistoryPage/HistoryCleaner.qml:115
msgid "Delete all"
msgstr ""

#. TRANSLATORS: this is the count of events grouped into this single item
#: src/qml/HistoryPage/SwipeItemDemo.qml:150
#: src/qml/HistoryPage/HistoryDelegate.qml:206
#, qt-format
msgid "(%1)"
msgstr ""

#: src/qml/HistoryPage/SwipeItemDemo.qml:164
msgid "Mobile"
msgstr ""

#: src/qml/HistoryPage/SwipeItemDemo.qml:189
#: src/qml/HistoryPage/HistoryDelegate.qml:65
#: src/qml/HistoryPage/HistoryDetailsPage.qml:340
msgid "Incoming"
msgstr ""

#: src/qml/HistoryPage/SwipeItemDemo.qml:255
msgid "Got it"
msgstr ""

#: src/qml/HistoryPage/SwipeItemDemo.qml:276
msgid "Swipe to reveal actions"
msgstr ""

#: src/qml/HistoryPage/SwipeItemDemo.qml:326
msgid "Swipe to delete"
msgstr ""

#: src/qml/HistoryPage/HistoryDelegate.qml:67
#: src/qml/HistoryPage/HistoryDetailsPage.qml:342
msgid "Outgoing"
msgstr ""

#: src/qml/HistoryPage/HistoryDelegate.qml:168
#: src/qml/HistoryPage/HistoryDetailsPage.qml:43
msgid "Private number"
msgstr ""

#: src/qml/HistoryPage/HistoryDelegate.qml:170
#: src/qml/HistoryPage/HistoryDetailsPage.qml:45
msgid "Unknown number"
msgstr ""

#. TRANSLATORS: this refers to which SIM card will be used as default for calls
#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:30
#, qt-format
msgid "Change all Call associations to %1?"
msgstr ""

#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:34
msgid "Change"
msgstr ""

#: src/qml/Dialogs/SetDefaultSIMCardDialog.qml:64
msgid "Don't ask again"
msgstr ""

#: src/qml/Dialogs/SimLockedDialog.qml:27
msgid "SIM Card is locked"
msgstr ""

#: src/qml/Dialogs/SimLockedDialog.qml:38
msgid ""
"Please unlock your SIM card to call or send a message. You can unlock your "
"SIM card from the Network Indicator at the top of the screen or by visiting "
"<a href=\"system_settings\">System Settings &gt; Security &amp; Privacy</a>."
msgstr ""

#: src/qml/Dialogs/SimLockedDialog.qml:50
msgid "Ok"
msgstr ""

#: src/qml/Dialogs/UssdErrorDialog.qml:28
msgid "Error"
msgstr ""

#: src/qml/Dialogs/UssdErrorDialog.qml:29
msgid "Invalid USSD code"
msgstr ""

#: src/qml/Dialogs/DisableFlightModeDialog.qml:28
msgid "You have to disable flight mode in order to make calls"
msgstr ""

#: src/qml/Dialogs/DisableFlightModeDialog.qml:32
msgid "Disable"
msgstr ""

#: src/qml/Dialogs/FlightModeProgressDialog.qml:29
msgid "Disabling flight mode"
msgstr ""

#: src/qml/Dialogs/UssdProgressDialog.qml:28
msgid "Please wait"
msgstr ""

#: src/qml/Dialogs/NoDefaultSIMCardDialog.qml:27
msgid "Switch to default SIM:"
msgstr ""

#: src/qml/Dialogs/NoDefaultSIMCardDialog.qml:58
msgid ""
"Select a default SIM for all outgoing calls. You can always alter your "
"choice in <a href=\"system_settings\">System Settings</a>."
msgstr ""

#: src/qml/Dialogs/NoDefaultSIMCardDialog.qml:70
msgid "No"
msgstr ""

#: src/qml/Dialogs/NoDefaultSIMCardDialog.qml:79
msgid "Later"
msgstr ""
